#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
    if (argc != 3 || strcmp(argv[1],"/?")==0)
    {
        printf("DHSC GetLines version 0.01.00\nMIT Licence\nTwitter: @DHeadshot\nMastodon: @dheadshot@mastodon.social\nExtract specific lines from a file\nUsage:\n  %s <filename> <range>\nWhere <filename> is the file to open and <range> are the line numbers to\nextract.  Every line number/subrange should be followed by a comma.\n  (e.g. %s example.txt 2,5-9,12,)\n",argv[0],argv[0]);
        if (strcmp(argv[1],"/?")==0) return 0;
        return 1;
    }
    long N = (1+strlen(argv[2]));
    long *m1tna = (long *) malloc(N*sizeof(long));
    if (!m1tna)
    {
        fprintf(stderr,"Out of Memory!\n");
        return 2;
    }
    int offset = 0, i,j=0,ato=0;
    long k;
    char *number = (char *) malloc((1+strlen(argv[2]))*sizeof(char));
    if (!number)
    {
        fprintf(stderr,"Out of Memory!\n");
        free(m1tna);
        return 2;
    }
    for (i=0;argv[2][i]!=0;i++)
    {
        if (isdigit(argv[2][i]))
        {
            number[j]=argv[2][i];
            j++;
        }
        else if (argv[2][i]==',')
        {
            number[j]=0;
            m1tna[offset] = atol(number);
            j=0;
            if (ato && offset>0)
            {
                k=m1tna[offset-1]+1;
                while(k<atol(number))
                {
                    m1tna[offset]=k;
                    offset++;
                    k++;
                    if (offset>N)
                    {
                        N+=(1+strlen(argv[2]));
                        m1tna = (long *) realloc((void *)m1tna,N*sizeof(long));
                        if (!m1tna)
                        {
                            fprintf(stderr,"Out of Memory!\n");
                            free(number);
                            return 2;
                        }
                    }
                }
                ato = 0;
                offset--;
            }
            offset++;
        }
        else if (argv[2][i]=='-')
        {
            number[j]=0;
            m1tna[offset] = atol(number);
            j=0;
            offset++;
            ato = 1;
        }
        else if (argv[2][i]<33)
        {
            //Ignore whitespace
        }
        else
        {
            //Error!
            fprintf(stderr,"Invalid character in range!\n");
            free(m1tna);
            free(number);
            return 3;
        }
    }
    if (ato)
    {
        m1tna[offset] = -2;
        offset++;
    }
    m1tna[offset] = -1;
    free(number);
#ifdef DEBUG
    for (i=0;i<=offset;i++)fprintf(stderr,"%ld, ",m1tna[i]);
#endif
    FILE *afp = fopen(argv[1],"r");
    if (!afp)
    {
        fprintf(stderr,"Could not open file %s!\n",argv[1]);
        free(m1tna);
        return 4;
    }
    i=0;
    char aline[1024];
    for (k=1;!feof(afp);k++)
    {
        if (!fgets(aline,1024,afp)) break;
        if (k==m1tna[i] || m1tna[i]==-2)
        {
            printf("%s",aline);
            if (m1tna[i]!=-2) i++;
            if (m1tna[i]==-1) break;
        }
    }
    if ((!feof(afp)) && m1tna[i]!=-1)
    {
        fprintf(stderr,"Error reading from file %s!\n",argv[1]);
        fclose(afp);
        free(m1tna);
        return 5;
    }
    fclose(afp);
    free(m1tna);
    return 0;
}
