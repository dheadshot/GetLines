GetLines
===========

(Win32)

MIT Licence, Copyright (c) 2022 DHeadshot's Software Creations
Twitter: @DHeadshot, Mastodon: @dheadshot@mastodon.social

Extract specific lines from a file.

Usage:
  getlines <filename> <range>
Where <filename> is the file to open and <range> are the line numbers to extract.  Every line number/subrange should be followed by a comma.
  (e.g. getlines example.txt 2,5-9,12,)
